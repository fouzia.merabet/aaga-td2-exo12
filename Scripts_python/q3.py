import re
import math

fr = open('res.data', 'r')
fw = open('res2.data', 'w')

while True:
	line = fr.readline()
	if line == '':
		break
	cols = re.split(' ', line)
	fw.write(cols[0] + ' ' + str(math.log2(int(cols[0]))) + ' ' + cols[1])
	
fr.close()
fw.close()
