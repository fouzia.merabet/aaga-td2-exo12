package aaga;

import java.util.ArrayList;
import java.util.Random;

public class Tree {

	class Node {
		public Node fg;
		public Node fd;

		public Node(Node fg, Node fd) {
			this.fg = fg;
			this.fd = fd;
		}
	}

	public Node root;

	public Tree(int n) {
		generate(n);
	}

	private void generate(int n) {

		ArrayList<Node> leafs = new ArrayList<Node>();
		Random rand = new Random();

		root = new Node(null, null);
		leafs.add(root);

		for (int i = 1; i < n; i++) {
			// Choose a leaf uniformly from leafs set
			int r = rand.nextInt(leafs.size());
			Node leaf = leafs.get(r);

			Node node = new Node(null, null);

			if (leaf.fg == null & leaf.fd != null) {
				leaf.fg = node;
				leafs.remove(r);
			} else if (leaf.fg != null & leaf.fd == null) {
				leaf.fd = node;
				leafs.remove(r);
			} else {
				boolean b = rand.nextBoolean();
				if (b) {
					leaf.fg = node;
				} else {
					leaf.fd = node;
				}
			}
			leafs.add(node);
		}

	}

	public int height(Node tree) {

		if (tree == null) {
			return 0;
		}
		return 1 + Math.max(height(tree.fg), height(tree.fd));
	}
	
	public int min_height(Node tree) {

		if (tree == null) {
			return 0;
		}
		return 1 + Math.min(height(tree.fg), height(tree.fd));
	}
	
	public void treeToString(Node tree, String tab) {

		if (tree != null) {
			System.out.println(tab + "#");
			treeToString(tree.fg, tab + " ");
			treeToString(tree.fd, tab + " ");
		}
	}

}
